#include <umrob/polynomial.h>
#include <math.h>
#include <iostream>
#include <Eigen/Dense>
#include <array>
#include <vector>

using namespace std;

namespace umrob {

Polynomial::Constraints& Polynomial::constraints() {
    return constraints_;
}

const Polynomial::Constraints& Polynomial::constraints() const {
    return constraints_;
}

// clang-format off
/* Simplified coefficient for xi = 0 and dx = xf-xi
 * a = -(12*yi - 12*yf + 6*dx*dyf + 6*dx*dyi - d2yf*dx^2 + d2yi*dx^2)/(2*dx^5)
 * b = (30*yi - 30*yf + 14*dx*dyf + 16*dx*dyi - 2*d2yf*dx^2 + 3*d2yi*dx^2)/(2*dx^4) 
 * c = -(20*yi - 20*yf + 8*dx*dyf + 12*dx*dyi - d2yf*dx^2 + 3*d2yi*dx^2)/(2*dx^3) 
 * d = d2yi/2 
 * e = dyi 
 * f = yi
 */
void Polynomial::computeCoefficients() {
    // TODO implement
    double dx = constraints_.xf - constraints_.xi;

    coefficients_.a = -((12*constraints_.yi) - (12*constraints_.yf) + (6*dx*constraints_.dyf) + (6*dx*constraints_.dyi) - (constraints_.d2yf*dx*dx) + (constraints_.d2yi*dx*dx))/(2*pow(dx,5));
    coefficients_.b = ((30*constraints_.yi) - (30*constraints_.yf) + (14*dx*constraints_.dyf) + (16*dx*constraints_.dyi) - (2*constraints_.d2yf*dx*dx) + (3*constraints_.d2yi*dx*dx)) / (2*pow(dx,4));
    coefficients_.c = -((-20*constraints_.yi) - (20*constraints_.yf) + (8*dx*constraints_.dyf) + (12*dx*constraints_.dyi) - (constraints_.d2yf*dx*dx) + (3*constraints_.d2yi*dx*dx)) / (2*pow(dx,3));
    coefficients_.d = constraints_.d2yi/2;
    coefficients_.e = constraints_.dyi;
    coefficients_.f = constraints_.yi;

//Another way to compute coefficients : this part has been added because when there is more than 1 segment it's not working well
// when xi != 0 the simplified coefficient formula cannot be used
    if(constraints_.yi != 0){

        Eigen::Matrix<double,6,6> A;
        Eigen::Matrix<double,6,6> Ainv;
        Eigen::Matrix<double,6,1> P;
        Eigen::Matrix<double,6,1> Coeff;

        A(0,0) = 0;
        A(0,1) = 0;
        A(0,2) = 0;
        A(0,3) = 0;
        A(0,4) = 0;
        A(0,5) = 1;

        A(1,0) = 0;
        A(1,1) = 0;
        A(1,2) = 0;
        A(1,3) = 0;
        A(1,4) = 1;
        A(1,5) = 0;

        A(2,0) = 0;
        A(2,1) = 0;
        A(2,2) = 0;
        A(2,3) = 2;
        A(2,4) = 0;
        A(2,5) = 0;

        A(3,0) = pow(dx,5);
        A(3,1) = pow(dx,4);
        A(3,2) = pow(dx,3);
        A(3,3) = pow(dx,2);
        A(3,4) = dx;
        A(3,5) = 1;

        A(4,0) = 5*pow(dx,4);
        A(4,1) = 4*pow(dx,3);
        A(4,2) = 3*pow(dx,2);
        A(4,3) = 2*dx;
        A(4,4) = 1;
        A(4,5) = 0;

        A(5,0) = 20*pow(dx,3);
        A(5,1) = 12*pow(dx,2);
        A(5,2) = 6*dx;
        A(5,3) = 2;
        A(5,4) = 0;
        A(5,5) = 0;

        P(0,0) = constraints_.yi;
        P(1,0) = constraints_.dyi;
        P(2,0) = constraints_.d2yi;
        P(3,0) = constraints_.yf;
        P(4,0) = constraints_.dyf;
        P(5,0) = constraints_.d2yf;

        Ainv = A.inverse();

        Coeff = Ainv*P;

        coefficients_.a = Coeff(0,0);
        coefficients_.b = Coeff(1,0);
        coefficients_.c = Coeff(2,0);
        coefficients_.d = Coeff(3,0);
        coefficients_.e = Coeff(4,0);
        coefficients_.f = Coeff(5,0);

    }

}
// clang-format on

//! y = ax^5 + bx^4 + cx^3 + dx^2 + ex + f
double Polynomial::evaluate(double x) {
    // TODO implement
    double y = (coefficients_.a*pow(x,5)) + (coefficients_.b*pow(x,4)) + (coefficients_.c*pow(x,3)) + (coefficients_.d*pow(x,2)) + (coefficients_.e*x) + (coefficients_.f);
    //y = coefficients_.f;
    return y;
}

//! dy = 5ax^4 + 4bx^3 + 3cx^2 + 2dx + e
double Polynomial::evaluateFirstDerivative(double x) {
    // TODO implement
    double dy = (5*coefficients_.a*pow(x,4)) + (4*coefficients_.b*pow(x,3)) + (3*coefficients_.c*pow(x,2)) + (2*coefficients_.d*x) + (coefficients_.e);
    return dy;
}

//! d2y = 20ax^3 + 12bx^2 + 6cx + 2d
double Polynomial::evaluateSecondDerivative(double x) {
    double d2y = (20*coefficients_.a*pow(x,3)) + (12*coefficients_.b*pow(x,2)) + (6*coefficients_.c*x) + (2*coefficients_.d);
    return d2y;
}

} // namespace umrob