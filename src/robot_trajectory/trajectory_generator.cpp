#include <umrob/trajectory_generator.h>
#include <fmt/format.h>
#include <numeric>
#include<time.h>
//#include <umrob/polynomial.h>
#include <iostream>

using namespace std;

namespace umrob {

TrajectoryGenerator::Segment::Segment(const Eigen::Vector6d& max_velocity,
                                      const Eigen::Vector6d& max_acceleration)
    : max_velocity{max_velocity}, max_acceleration{max_acceleration} {
}

//Constructor of the class TrajectoryGenerator
TrajectoryGenerator::TrajectoryGenerator(double time_step)
    : time_step_{time_step} {
    reset();
}

//All the different methods (function) of the class TrajectoryGenerator
void TrajectoryGenerator::startFrom(const Eigen::Affine3d& pose) {
    reset();
    last_waypoint_ = pose;
    //target_pose_ = pose;
}

void TrajectoryGenerator::addWaypoint(
    const Eigen::Affine3d& pose,
    const Eigen::Vector6d& max_velocity,
    const Eigen::Vector6d& max_acceleration) {
    // TODO implement
    target_pose_ = pose;

//Create a new segment for the new WayPoint to reach
    Segment s;
    s.max_acceleration = max_acceleration;
    s.max_velocity = max_velocity;

//Compute the segment duration with the given paramters (max acceleration & max velocity)
    computeSegmentDuration(s); 

//Create 3 differents polynomials : one for eache axis : x,y,z
    Polynomial px;
    Polynomial py;
    Polynomial pz;

//Set to all polynomials constraints
    px.constraints().xf = s.duration;
    px.constraints().xi = 0;
    px.constraints().yi = last_waypoint_(0,0);
    px.constraints().yf = target_pose_(0,0);

    py.constraints().xf = s.duration;
    py.constraints().xi = 0;
    py.constraints().yf = target_pose_(1,0);
    py.constraints().yi = last_waypoint_(1,0);

    pz.constraints().xf = s.duration;
    pz.constraints().xi = 0;
    pz.constraints().yf = target_pose_(2,0);
    pz.constraints().yi = last_waypoint_(2,0);

    s.polynomials.at(0) = px;
    s.polynomials.at(1) = py;
    s.polynomials.at(2) = pz;

    computeSegmentParameters(s);

//Save the segment in segments_ vector (belongs to trajectory_generator class)
    segments_.push_back(s);

//The last_waypoint_ becomes the target_pose : needed if other segment has to be compute
    last_waypoint_ = target_pose_; //Maybe delete this line

}

//Another method to add wayPoint
void TrajectoryGenerator::addWaypoint(
    const Eigen::Affine3d& pose,
    double duration) {
    // TODO implement
    target_pose_ = pose;

//Create a segment for the current wayPoint to reach
    Segment s;
    s.duration = duration;

//Create 3 differents polynomials : one for each axis : x,y,z
    Polynomial px;
    Polynomial py;
    Polynomial pz;

//Set to all polynomials their constraints
    px.constraints().xf = duration;
    px.constraints().xi = 0;
    px.constraints().yf = target_pose_(0,0);
    px.constraints().yi = last_waypoint_(0,0);

    py.constraints().xf = duration;
    py.constraints().xi = 0;
    py.constraints().yf = target_pose_(1,0);
    py.constraints().yi = last_waypoint_(1,0);

    pz.constraints().xf = duration;
    pz.constraints().xi = 0;
    pz.constraints().yf = target_pose_(2,0);
    pz.constraints().yi = last_waypoint_(2,0);

    s.polynomials.at(0) = px;
    s.polynomials.at(1) = py;
    s.polynomials.at(2) = pz;

    computeSegmentParameters(s);

//Save the current segment in segments_ vector's
    segments_.push_back(s);

//The last_waypoint_ become the target pose : needed if adding new segment
    last_waypoint_ = target_pose_;
}

//Update method : allow to evaluate the next_pose for the actual segment
TrajectoryGenerator::State TrajectoryGenerator::update() {
    if (state_ == State::TrajectoryCompleted) {
        return state_;
    } else {
        state_ = State::ReachingWaypoint;
    }

    auto evaluate = [this]() {
        Eigen::Vector6d next_pose_vec;

        // TODO evaluate the polynomial and its derivatives into next_pose_vec,
        // target_velocity_ and target_acceleration_

//Get the actuam segment
        Segment s = segments_.at(current_segment_idx_);

//Compute the next pose, target_velocity and target_acceleration for the current segment
        next_pose_vec(0,0) = s.polynomials.at(0).evaluate(s.current_time);
        next_pose_vec(1,0) = s.polynomials.at(1).evaluate(s.current_time);
        next_pose_vec(2,0) = s.polynomials.at(2).evaluate(s.current_time);

        target_velocity_(0,0) = s.polynomials.at(0).evaluateFirstDerivative(s.current_time);
        target_velocity_(1,0) = s.polynomials.at(1).evaluateFirstDerivative(s.current_time);
        target_velocity_(2,0) = s.polynomials.at(2).evaluateFirstDerivative(s.current_time);

        target_acceleration_(0,0) = s.polynomials.at(0).evaluateSecondDerivative(s.current_time);
        target_acceleration_(1,0) = s.polynomials.at(1).evaluateSecondDerivative(s.current_time);
        target_acceleration_(2,0) = s.polynomials.at(2).evaluateSecondDerivative(s.current_time);

        target_pose_.translation() = next_pose_vec.head<3>();
        target_pose_.linear() =
            Eigen::Quaterniond::fromAngles(next_pose_vec.tail<3>())
                .toRotationMatrix();
    };

    evaluate();


    currentSegment().current_time += time_step_;
    if (currentSegment().current_time > currentSegment().duration+time_step_) {
        // TODO switch to next segment and re-evaluate if needed

//Switch to next segment
        state_ = State::WaypointReached;
        current_segment_idx_ += 1;

//Evaluate if the trajectory is completed or not
        if(current_segment_idx_ >=segments_.size()){
            state_ = State::TrajectoryCompleted;
        }
    }

    return state_;
}

const Eigen::Affine3d& TrajectoryGenerator::targetPose() const {
    return target_pose_;
}

const Eigen::Vector6d& TrajectoryGenerator::targetVelocity() const {
    return target_velocity_;
}

const Eigen::Vector6d& TrajectoryGenerator::targetAcceleration() const {
    return target_acceleration_;
}

TrajectoryGenerator::State TrajectoryGenerator::state() const {
    return state_;
}

double TrajectoryGenerator::duration() const {
    // TODO implement
    double duration = 0;
    
    for(long unsigned int i =0; i<segments_.size(); i++){
        duration = duration + segments_.at(i).current_time;
    }
    return duration;
}

void TrajectoryGenerator::reset() {
    state_ = State::Idle;
    last_waypoint_.setIdentity();
    segments_.clear();
    current_segment_idx_ = 0;

    //target_pose_ = last_waypoint_;
    target_velocity_.setZero();
    target_acceleration_.setZero();
}

void TrajectoryGenerator::setSegmentConstraints(const Eigen::Affine3d& from,
                                                const Eigen::Affine3d& to,
                                                Segment& segment) {
    Eigen::Vector6d from_vec, to_vec;
    from_vec << from.translation(),
        Eigen::Quaterniond(from.linear()).getAngles();
    to_vec << to.translation(), Eigen::Quaterniond(to.linear()).getAngles();

    for (size_t i = 0; i < 6; i++) {
        auto& poly = segment.polynomials[i];
        auto& cstr = poly.constraints();
        cstr.xi = 0;
        cstr.xf = 0;
        cstr.yi = from_vec(i);
        cstr.yf = to_vec(i);
    }
}

//Compute the segmentDuration with the next formulas :
//! Tmin_vel = 30Δy/16vmax
//! Tmin_acc = sqrt(10sqrt(3)Δy/3amax)
void TrajectoryGenerator::computeSegmentDuration(
    Segment& segment) {
    // TODO implement

//The computation has to be done for each axis
    double deltaY_x = abs(target_pose_(0,0) - last_waypoint_(0,0));
    double deltaY_y = abs(target_pose_(1,0) - last_waypoint_(1,0));
    double deltaY_z = abs(target_pose_(2,0) - last_waypoint_(2,0));
    
    double Tmin_vel_x = (30*deltaY_x)/(16*segment.max_velocity(0,0));
    double Tmin_vel_y = (30*deltaY_y)/(16*segment.max_velocity(1,0));
    double Tmin_vel_z = (30*deltaY_z)/(16*segment.max_velocity(2,0));

    double Tmin_acc_x = sqrt((10*sqrt(3)*deltaY_x)/(3*segment.max_acceleration(0,0)));
    double Tmin_acc_y = sqrt((10*sqrt(3)*deltaY_y)/(3*segment.max_acceleration(1,0)));
    double Tmin_acc_z = sqrt((10*sqrt(3)*deltaY_z)/(3*segment.max_acceleration(2,0)));

//To get the maximum value between all axis
    double Tmin_vel = std::max({Tmin_vel_x, Tmin_vel_y, Tmin_vel_z});
    double Tmin_acc = std::max({Tmin_acc_x, Tmin_acc_y, Tmin_acc_z});

//Finally, the duration is equals to the maximum values between acc & vel
    segment.duration = std::max(Tmin_vel, Tmin_acc);

    std::cout << "\n Duration for this segment : " << segment.duration << std::endl;
}

void TrajectoryGenerator::computeSegmentParameters(
    Segment& segment) {
    // TODO compute the polynomials coefficients for the current duration
    segment.polynomials.at(0).computeCoefficients();
    segment.polynomials.at(1).computeCoefficients();
    segment.polynomials.at(2).computeCoefficients();

}

//Return the currentSegment
TrajectoryGenerator::Segment& TrajectoryGenerator::currentSegment() {
    return segments_.at(current_segment_idx_);
}

} // namespace umrob
