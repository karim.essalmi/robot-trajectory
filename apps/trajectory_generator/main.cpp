#include <umrob/trajectory_generator.h>

//#include "trajectory_generator.h"

#include <fmt/format.h>
#include <fmt/color.h>
#include <fmt/ostream.h>

#include <thread>
#include <chrono>
#include <iostream>

int main(int argc, char** argv) {
    constexpr auto time_step = 0.01;

    // Create a trajectory generator with a given time step
    umrob::TrajectoryGenerator trajectory(time_step);

    // Create the maximum velocites and acceleration vectors
    Eigen::Vector6d vmax = Eigen::Vector6d::Ones();
    Eigen::Vector6d amax = Eigen::Vector6d::Ones();

    // Create a default identity pose
    auto desiredPose = Eigen::Affine3d::Identity();
    auto desiredPoseConstraints = Eigen::Affine3d::Identity();
    auto startPose = Eigen::Affine3d::Identity();
    double print_timer{0};

//The desired duration time
    double desiredDuration1 = 1.;
    double desiredDuration2 = 5.;

//The startPose : where the robot will start
    startPose(0,0) = 0;
    startPose(1,0) = 0;
    startPose(2,0) = 0;

    trajectory.startFrom(startPose);


//The desired points to reach
    desiredPose(0,0) = 2;
    desiredPose(1,0) = 1;
    desiredPose(2,0) = 3;

    desiredPoseConstraints(0,0) = 0;
    desiredPoseConstraints(1,0) = 2;
    desiredPoseConstraints(2,0) = 4;

//Add 3 differents wayPoints to reach
    trajectory.addWaypoint(desiredPose, desiredDuration1);
    trajectory.addWaypoint(desiredPoseConstraints, vmax, amax);
    trajectory.addWaypoint(desiredPose, desiredDuration2);

    // Loop while we haven't reached the end of the trajectory
    while (trajectory.state() !=
           umrob::TrajectoryGenerator::State::TrajectoryCompleted) {
        // Print a message if we just reached a waypoint
        if (trajectory.update() ==
            umrob::TrajectoryGenerator::State::WaypointReached) {
            fmt::print(fg(fmt::color::red) | fmt::emphasis::bold, "\n{:*^50}\n",
                       " Waypoint reached ");
        }

        // Print the current position, velocity and acceleration targets every
        // 0.5s
        print_timer += time_step;
        if (print_timer > 0.5) {
            print_timer = 0;
            fmt::print("\n{:*^50}\n", " Current targets ");
            fmt::print("position: {}\n",
                       trajectory.targetPose().translation().transpose());
            fmt::print("velocity: {}\n",
                       trajectory.targetVelocity().head<3>().transpose());
            fmt::print("acceleration: {}\n",
                       trajectory.targetAcceleration().head<3>().transpose());
        }

        // Sleep for the duration of a time step
        std::this_thread::sleep_for(std::chrono::duration<double>(time_step));
    }
 
    fmt::print(fg(fmt::color::red) | fmt::emphasis::bold, "\n{:*^50}\n",
               " Trajectory completed ");

//When the trajectory is done : Print the total trajectory duration
    std::cout << "Trajectory duration : "<< trajectory.duration() <<  "s" <<std::endl;
}
