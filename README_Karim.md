# HOW TO LAUNCH THE CODE :
### 1. Clone the project :
```
	$git clone git@gitlab.com:karim.essalmi/robot-trajectory.git
	$cd robot-trajectory/
```
### 2. Remove 'CMakeCache.txt' & 'conan.cmake' files & 'CMakeFiles' folder : if they exist
```
	$rm CMakeCache.txt
	$rm conan.cmake
	$rm -rf CMakeFiles

```

**NB** : step 2 is **optional** : normally they are already deleted

### 3. Configure CMake : 
```
	$cmake --configure .
```
### 4. To compile the code :
```
	$cmake --build .

```

**NB** : don't forget the '.' in 'cmake --build .' and 'cmake --configure .' commands

### 5. To execute the code :
```
	$./bin/trajectory_generator
```
	
# WHAT THE CODE DO :

The program reach three different waypoints by using two different methods : 

- The first point to reach is located in (2,1,3) position (x,y,z), this first waypoint has to be reach in 1 second.

- The second point to reach is located in (0,2,4) position (x,y,z), the duration to reach this waypoint is compute by setting maximum velocity and acceleration

- The last point to reach is located in (2,1,3), it has to be reached in 5 seconds


All the parameters (desiredPose, duration_time, vmax, amax..) can be changed by the user in main.cpp file.


**NB** : a video named "program_running" shows how to launch the code and shows also what happened while the code is running.

Also the user can added an orientation to reach.
